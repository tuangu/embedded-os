#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <string.h>
#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "user_vcom.h"

#define CHAR_LIMIT 60

void setupHardware();

SemaphoreHandle_t countingSemaphore;

void vReceiverTask(void *vParameters);
void vOracleTask(void *vParameters);

int main(void) {

    setupHardware();

    // queue up to 10 events
    countingSemaphore = xSemaphoreCreateCounting(10, 0);

    xTaskCreate(vReceiverTask, "Receiver  Task", configMINIMAL_STACK_SIZE * 3,
            NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vOracleTask, "Oracle Task", configMINIMAL_STACK_SIZE * 3, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(cdc_task, "CDC", configMINIMAL_STACK_SIZE * 2, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

/*
 * Read characters from USB port until \r or \n or 60 characters is received
 * Echo back to the USB port, preceded by [You]
 * Notify the oracle task if `?` is received
 */
void vReceiverTask(void *vParameters) {
    int index = 0;
    char prefix[] = "[You]";
    bool isQuestion = false;

    while (1) {
        char recvBuf[RCV_BUFSIZE];
        int len = USB_receive((uint8_t *) recvBuf, RCV_BUFSIZE);

        if (index == 0)
            USB_send((uint8_t *) prefix, strlen(prefix));

        for (uint8_t i = 0; i < len; i++) {
            if (recvBuf[i] == '?') {
                isQuestion = true;
            }

            if ((index == CHAR_LIMIT) || (recvBuf[i] == '\r')) {
                char endline[] = "\r\n";
                USB_send((uint8_t *) endline, 2);

                if (isQuestion)
                    xSemaphoreGive(countingSemaphore);

                index = 0;
                isQuestion = false;

                continue;
            }

            USB_send((uint8_t *) (recvBuf + i), 1);
            index++;
        }
    }
}

void vOracleTask(void *vParameters) {
    char *answers[7] = { "After she left, I feel cold.\r\n",
            "She waits. War ended. No homecoming.\r\n",
            "Two yesterday. One today. None tomorrow.\r\n",
            "The sound of clocks and crickets.\r\n",
            "Where did this cow come from?\r\n",
            "Woke up, then took a rest.\r\n", "At least my dog loves me.\r\n", };

    while (1) {
        char thinking[] = "[Oracle] I find your lack of faith disturbing\r\n";
        char buffer[80];

        if (xSemaphoreTake(countingSemaphore, 0) == pdPASS) {
            int8_t len = snprintf(buffer, 80, "[Oracle] %s",
                    answers[(xTaskGetTickCount() % 7)]);
            USB_send((uint8_t *) thinking, strlen(thinking));
            vTaskDelay(configTICK_RATE_HZ * 3);
            USB_send((uint8_t *) buffer, len);
            vTaskDelay(configTICK_RATE_HZ * 2);
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
