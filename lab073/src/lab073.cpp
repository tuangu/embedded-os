#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdlib.h>
#include <string.h>

#include "DigitalIoPin.h"
#include "Color.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "user_vcom.h"

void vSetupHardware();
void SCT0_Setup();
void SCT1_Setup();

void vLedTask(void *vParameters);
void vSerialTask(void *vParameters);

Color* getColor(char *buffer);

QueueHandle_t colorQueue;

int main(void) {

    vSetupHardware();

    colorQueue = xQueueCreate(10, sizeof(Color *));

    xTaskCreate(vLedTask, "LED Task", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vSerialTask, "Serial Task", configMINIMAL_STACK_SIZE * 2,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(cdc_task, "CDC", configMINIMAL_STACK_SIZE * 2, NULL,
                (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    return 0;
}

void vSetupHardware() {
    SystemCoreClockUpdate();

    Board_Init();

    Chip_SCT_Init(LPC_SCT0);
    Chip_SCT_Init(LPC_SCT1);
    SCT0_Setup();
    SCT1_Setup();

    Chip_SWM_Init();
    Chip_SWM_MovablePortPinAssign(SWM_SCT0_OUT0_O, 0, 3); // LED Green
    Chip_SWM_MovablePortPinAssign(SWM_SCT0_OUT1_O, 1, 1); // LED Blue
    Chip_SWM_MovablePortPinAssign(SWM_SCT1_OUT0_O, 0, 25); // LED Red
}

void SCT0_Setup() {
    LPC_SCT0->CONFIG |= (1 << 17) | (1 << 18); // two 16-bit timers, auto limit L, H
    LPC_SCT0->CTRL_L |= (72 - 1) << 5; // set prescaler, SCTimer/PWM clock = 1 MHz
    LPC_SCT0->CTRL_H |= (72 - 1) << 5; // set prescaler, SCTimer/PWM clock = 1 MHz

    /* The on-board leds work with inverted logic. */
    LPC_SCT0->MATCHREL[0].L = 1000-1; // match 0 @ 1 kHz PWM freq
    LPC_SCT0->MATCHREL[1].L = 1000; // match 1 used for duty cycle,
    LPC_SCT0->MATCHREL[0].H = 1000-1; // match 0 @ 1 kHz PWM freq
    LPC_SCT0->MATCHREL[1].H = 1000; // match 1 used for duty cycle,

    LPC_SCT0->EVENT[0].STATE = 0xFFFFFFFF; // event 0 happens in all states
    LPC_SCT0->EVENT[0].CTRL = (0 << 4) | (1 << 12); // L state, match 0 condition only

    LPC_SCT0->EVENT[1].STATE = 0xFFFFFFFF; // event 1 happens in all states
    LPC_SCT0->EVENT[1].CTRL = (0 << 4) | (1 << 0) | (1 << 12); // L state, match 1 condition only

    LPC_SCT0->EVENT[2].STATE = 0xFFFFFFFF; // event 2 happens in all states
    LPC_SCT0->EVENT[2].CTRL = (1 << 4) | (1 << 12); // H state, match 0 condition only

    LPC_SCT0->EVENT[3].STATE = 0xFFFFFFFF; // event 3 happens in all states
    LPC_SCT0->EVENT[3].CTRL = (1 << 4) | (1 << 0) | (1 << 12); // H state, match 1 condition only

    /* event 0 will set SCT0_OUTx, event 1 will clear SCT0_OUTx */
    LPC_SCT0->OUT[0].SET = (1 << 0); // event 0 will set SCT0_OUTx
    LPC_SCT0->OUT[0].CLR = (1 << 1); // event 1 will clear SCT0_OUTx
    LPC_SCT0->OUT[1].SET = (1 << 2); // event 2 will set SCT0_OUTx
    LPC_SCT0->OUT[1].CLR = (1 << 3); // event 3 will clear SCT0_OUTx

    /* unhalt it by clearing bit 2 of CTRL reg */
    LPC_SCT0->CTRL_L &= ~(1 << 2);
    LPC_SCT0->CTRL_H &= ~(1 << 2);
}

void SCT1_Setup() {
    LPC_SCT1->CONFIG |= (1 << 17); // two 16-bit timers, auto limit
    LPC_SCT1->CTRL_L |= (72-1) << 5; // set prescaler, SCTimer/PWM clock = 1 MHz

    /* The on-board leds work with inverted logic. */
    LPC_SCT1->MATCHREL[0].L = 1000-1; // match 0 @ 1 kHz PWM freq
    LPC_SCT1->MATCHREL[1].L = 1000; // match 1 used for duty cycle, = 5%

    LPC_SCT1->EVENT[0].STATE = 0xFFFFFFFF; // event 0 happens in all states
    LPC_SCT1->EVENT[0].CTRL = (1 << 12); // match 0 condition only

    LPC_SCT1->EVENT[1].STATE = 0xFFFFFFFF; // event 1 happens in all states
    LPC_SCT1->EVENT[1].CTRL = (1 << 0) | (1 << 12); // match 1 condition only

    LPC_SCT1->OUT[0].SET = (1 << 0); // event 0 will set SCTx_OUT0
    LPC_SCT1->OUT[0].CLR = (1 << 1); // event 1 will clear SCTx_OUT0

    LPC_SCT1->CTRL_L &= ~(1 << 2); // unhalt it by clearing bit 2 of CTRL reg
}

void vLedTask(void *vParameters) {
    Color *clr;

    while (1) {
        if (xQueueReceive(colorQueue, &clr, portMAX_DELAY) == pdPASS) {
            LPC_SCT1->MATCHREL[1].L = 1000 - clr->red*1000;
            LPC_SCT0->MATCHREL[1].L = 1000 - clr->green*1000;
            LPC_SCT0->MATCHREL[1].H = 1000 - clr->blue*1000;

            delete clr;
        }
    }
}

/*
 * Reads commands from the USB serial port (vcom_cdc) and sets color of the leds.
 * Command: rgb #rrggbb
 */
void vSerialTask(void *vParameters) {

    while (1) {
        char buffer[RCV_BUFSIZE] = "";
        int idx = 0;
        while (idx < RCV_BUFSIZE) {
            int len = USB_receive((uint8_t *) (buffer + idx), RCV_BUFSIZE);

            /* Find the first occurrence of '\r' */
            char *pos = strstr((buffer + idx), "\r");
            if (pos != NULL) {
                USB_send((uint8_t *) (buffer + idx), pos - (buffer + idx) + 1);
                USB_send((uint8_t *) "\n", 1);
                break;
            } else {
                USB_send((uint8_t *) (buffer + idx), len);
            }

            idx += len;
        }

        Color *clr = getColor(buffer);

        if (clr != NULL)
            xQueueSend(colorQueue, &clr, portMAX_DELAY);
    }
}

Color* getColor(char *buffer) {
    if (strncmp(buffer, "rgb #", 5) == 0) {
        Color *ret = new Color();
        int hexValue = strtol(buffer+5, NULL, 16);
        ret->red = ((hexValue >> 16) & 0xFF) / 255.0;
        ret->green = ((hexValue >> 8) & 0xFF) / 255.0;
        ret->blue = (hexValue & 0xFF) / 255.0;
        return ret;
    } else
        return NULL;
}


/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
