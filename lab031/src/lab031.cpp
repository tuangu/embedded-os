#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define BUTTON_PRESSED 0

void setupHardware();
void vSerialCountTask(void *vParameters);
void vButtonTask(void *vParameters);
void vSerialPrintTask(void *vParameters);

QueueHandle_t lineCharsQueue;
SerialLog serial;

int main(void) {

    setupHardware();
    lineCharsQueue = xQueueCreate(10, sizeof(int));

    if (lineCharsQueue != NULL) {
        xTaskCreate(vSerialCountTask, "Serial Count Task",
                configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1),
                (TaskHandle_t *) NULL);

        xTaskCreate(vButtonTask, "Button Task", configMINIMAL_STACK_SIZE, NULL,
                (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

        xTaskCreate(vSerialPrintTask, "Serial Print Task", 512, NULL,
                (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);
    }

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vSerialCountTask(void *vParameters) {
    int charCounter = 0;

    while (1) {
        int c = serial.read();
        if (c != EOF) {
            charCounter++;
            serial.write((char *) &c);

            if (c == '\r') {
                serial.write("\n");
                charCounter--;
                if (charCounter == 0)
                    continue;
                if (xQueueSendToBack(lineCharsQueue, &charCounter,
                        portMAX_DELAY) == pdPASS)
                    charCounter = 0;
            }
        }
    }
}

void vButtonTask(void *vParameters) {
    DigitalIoPin button(0, 17, DigitalIoPin::pullup, false);
    int buttonValue = -1;

    while (1) {
        if (button.read() == BUTTON_PRESSED)
            xQueueSendToBack(lineCharsQueue, &buttonValue, portMAX_DELAY);
    }
}

void vSerialPrintTask(void *vParameters) {
    int sum = 0;
    int receivedValue;
    char buffer[40];

    while (1) {
        if (xQueueReceive(lineCharsQueue, &receivedValue,
                portMAX_DELAY) == pdPASS) {
            if (receivedValue == -1) {
                if (sum != 0) {
                    snprintf(buffer, 30, "You have typed %d characters", sum);
                    sum = 0;
                    serial.write(buffer);
                    serial.write("\r\n");
                }
            } else {
                sum += receivedValue;
            }
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
