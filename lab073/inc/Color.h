/*
 * Color.h
 *
 *  Created on: Sep 19, 2017
 *      Author: tuanngu
 */

#ifndef COLOR_H_
#define COLOR_H_

struct Color {
    float red;
    float green;
    float blue;
};

#endif /* COLOR_H_ */
