#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdlib.h>
#include <time.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

void setupHardware();

void vTask1(void *vParameters);
void vTask2(void *vParameters);
void vTask3(void *vParameters);
void vTask4(void *vParameters);

SerialLog serial;
EventGroupHandle_t eBtn;


int main(void) {

    setupHardware();

    srand(time(NULL));

    eBtn = xEventGroupCreate();

    xTaskCreate(vTask1, "Task 1", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask2, "Task 2", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask3, "Task 3", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask4, "Task 4", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vTask1(void *vParameters) {
    DigitalIoPin btn1(0, 17, DigitalIoPin::pullup, true);

    while (1) {
        if (btn1.read()) {
            xEventGroupSetBits(eBtn, (1 << 0));
        }

        vTaskDelay(20);
    }
}

void vTask2(void *vParameters) {

    EventBits_t eventBit;
    const EventBits_t bitToWait = (1 << 0);

    eventBit = xEventGroupWaitBits(eBtn, bitToWait, pdFALSE, pdTRUE, portMAX_DELAY);

    while (1) {
        char buffer[16];

        float k = rand();
        while (k > 1)
            k = k / 10.0;
        TickType_t delayTime = configTICK_RATE_HZ + configTICK_RATE_HZ*k;

        vTaskDelay(delayTime);

        if ((eventBit & bitToWait) != 0) {
            snprintf(buffer, 16, "Task 2 %lu \r\n", xTaskGetTickCount());
            serial.write(buffer);
        }
    }
}

void vTask3(void *vParameters) {

    EventBits_t eventBit;
    const EventBits_t bitToWait = (1 << 0);

    eventBit = xEventGroupWaitBits(eBtn, bitToWait, pdFALSE, pdTRUE, portMAX_DELAY);

    while (1) {
        char buffer[16];

        float k = rand();
        while (k > 1)
            k = k / 10.0;
        TickType_t delayTime = configTICK_RATE_HZ + configTICK_RATE_HZ*k;

        vTaskDelay(delayTime);

        if ((eventBit & bitToWait) != 0) {
            snprintf(buffer, 16, "Task 3 %lu \r\n", xTaskGetTickCount());
            serial.write(buffer);
        }
    }
}

void vTask4(void *vParameters) {

    EventBits_t eventBit;
    const EventBits_t bitToWait = (1 << 0);

    eventBit = xEventGroupWaitBits(eBtn, bitToWait, pdFALSE, pdTRUE, portMAX_DELAY);

    while (1) {
        char buffer[16];

        float k = rand();
        while (k > 1)
            k = k / 10.0;
        TickType_t delayTime = configTICK_RATE_HZ + configTICK_RATE_HZ*k;

        vTaskDelay(delayTime);

        if ((eventBit & bitToWait) != 0) {
            snprintf(buffer, 16, "Task 4 %lu \r\n", xTaskGetTickCount());
            serial.write(buffer);
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
