/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "task.h"

static void prvSetupHardware();
static void vLEDTask1(void *pvParameters);
static void vLEDTask2(void *pvParameters);

int main(void) {

    prvSetupHardware();

    xTaskCreate(vLEDTask1, "vTaskLed1", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vLEDTask2, "vTaskLed2", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    // Start the scheduler
    vTaskStartScheduler();

    return 1;
}

static void prvSetupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
    Board_LED_Set(1, false);
}

static void vLEDTask1(void *pvParameters) {
    bool LedState = true;
    static int counter = 18;

    static TickType_t dot = (configTICK_RATE_HZ / 10);
    static TickType_t dash = dot * 3;

    while (1) {
        Board_LED_Set(0, LedState);
        LedState = !LedState;

        if (counter == 12 || counter == 10 || counter == 8)
            vTaskDelay(dash);
        else
            vTaskDelay(dot);

        if (counter-- == 0) {
            counter = 18;
            LedState = true;
        }
    }
}

static void vLEDTask2(void *pvParameters) {
    bool LedState = false;

    while (1) {
        Board_LED_Set(1, LedState);

//		if (LedState) {
//			vTaskDelay(configTICK_RATE_HZ / 1);
//		} else {
//			vTaskDelay(configTICK_RATE_HZ * 24);
//		}
        vTaskDelay(configTICK_RATE_HZ * 2.4);

        LedState = !LedState;
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statictics collection */
