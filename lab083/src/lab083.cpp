#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
#include <stdio.h>

#include "ButtonEvent.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define BUTTON_1_PORT 0
#define BUTTON_1_PIN 17
#define BUTTON_2_PORT 1
#define BUTTON_2_PIN 11
#define BUTTON_3_PORT 1
#define BUTTON_3_PIN 9

void vSetupHardware();
void Button_Setup();

void vBtn3Task(void *vParameters);
void vLockerTask(void *vParameters);

xQueueHandle qBtn;

int main(void) {

    vSetupHardware();

    qBtn = xQueueCreate(10, sizeof(ButtonEvent));

    xTaskCreate(vBtn3Task, "Button 3 Task", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vLockerTask, "Locker Task", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 2UL), (TaskHandle_t *) NULL);

    vQueueAddToRegistry(qBtn, "Button Queue");

    vTaskStartScheduler();

    return 0;
}

void vBtn3Task(void *vParameters) {
    DigitalIoPin btn3(BUTTON_3_PORT, BUTTON_3_PIN, DigitalIoPin::pullup, true);

    const BaseType_t THREE_SECONDS = 3 * configTICK_RATE_HZ;
    BaseType_t lastPress = 0;
    bool lastReading = false;

    while (1) {
        bool btn3Reading = btn3.read();
        BaseType_t readingTime = xTaskGetTickCount();

        if (btn3Reading != lastReading)
            lastPress = readingTime;

        if (lastReading && btn3Reading) {
            if ((readingTime - lastPress) > THREE_SECONDS) {
                ButtonEvent btn;
                btn.pin = 3;
                btn.tickCount = xTaskGetTickCount();
                xQueueSendToBack(qBtn, &btn, portMAX_DELAY);
            }
        }

        lastReading = btn3Reading;
    }
}

void vLockerTask(void *vParameters) {

    const BaseType_t TIME_OUT = 15*configTICK_RATE_HZ; // 15s
    const BaseType_t FILTER_TIME = configTICK_RATE_HZ / 4; // 250ms

    uint8_t pattern[8] = {1, 1, 1, 1, 0, 1, 1, 0};
    uint8_t p = 0;
    uint8_t input[8] = {2, 2, 2, 2, 2, 2, 2, 2};
    uint8_t i = 0;

    bool isLearning = false;
    BaseType_t lastPress = 0;
    ButtonEvent recv;

    Board_LED_Set(0, true);

    while (1) {
        BaseType_t status = xQueueReceive(qBtn, &recv, TIME_OUT);

        // Ignore all previous keys if nothing is received within 15s
        if (status == errQUEUE_EMPTY) {
            for (int j = 0; j < 8; j++)
                input[j] = 2;
            i = 0;
            continue;
        }

        // Eliminate false presses from switch bounce
        if ((recv.tickCount - lastPress) < FILTER_TIME)
            continue;
        lastPress = recv.tickCount;

        switch (recv.pin) {
        case 1:
        case 2:
            if (isLearning) { // Learning mode
                if (recv.pin == 1)
                    pattern[p++] = 0;
                else
                    pattern[p++] = 1;
                Board_LED_Set(2, false);
                vTaskDelay(50);
                Board_LED_Set(2, true);

                // New pattern recorded
                if (p == 8) {
                    isLearning = false;
                    p = 0;

                    // Reset input
                    for (int j = 0; j < 8; j++)
                        input[j] = 2;
                    i = 0;

                    // LEDs
                    Board_LED_Set(2, false);
                    Board_LED_Set(0, true);
                }
            } else { // Unlocking mode
                if (recv.pin == 1)
                    input[i] = 0;
                else
                    input[i] = 1;
                Board_LED_Set(0, false);
                vTaskDelay(50);
                Board_LED_Set(0, true);

                // Compare input with pattern
                bool unlocked = true;
                for (uint8_t c = 0; c < 8; c++)
                    if (input[(i+c+1) % 8] != pattern[c]) {
                        unlocked = false;
                        break;
                    }

                // Unlock the door
                if (unlocked) {
                    // Reset input
                    for (int j = 0; j < 8; j++)
                        input[j] = 2;
                    i = 0;

                    // LEDs
                    Board_LED_Set(1, true);
                    Board_LED_Set(0, false);
                    vTaskDelay(5*configTICK_RATE_HZ);
                } else {
                    // Increase counter of input if pattern don't match with input
                    i = (i == 7) ? 0 : i+1;
                }

                Board_LED_Set(1, false);
                Board_LED_Set(0, true);
            }

            break;
        case 3:
            isLearning = true;
            p = 0;

            // LEDs
            Board_LED_Set(2, true);
            Board_LED_Set(0, false);
            Board_LED_Set(1, false);
            break;
        default:
            break;
        }
    }
}

void vSetupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Button_Setup();
}

void Button_Setup() {
    /* Set pin back to GPIO */
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_1_PORT, BUTTON_1_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_2_PORT, BUTTON_2_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);

    /* Configure GPIO pin as input */
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_1_PORT, BUTTON_1_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_2_PORT, BUTTON_2_PIN);

    /* Enable PININT clock */
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_PININT);

    /* Reset the PININT block */
    Chip_SYSCTL_PeriphReset(RESET_PININT);

    /* Configure interrupt channel for the GPIO pin in INMUX block */
    Chip_INMUX_PinIntSel(1, BUTTON_1_PORT, BUTTON_1_PIN);
    Chip_INMUX_PinIntSel(2, BUTTON_2_PORT, BUTTON_2_PIN);

    /* Configure channel interrupt as edge sensitive and falling edge interrupt */
    LPC_GPIO_PIN_INT->IST = (1 << 1) | (1 << 2); // Clear interrupt status
    LPC_GPIO_PIN_INT->ISEL &= ~(1 << 1) | ~(1 << 2); // Configure the pins as edge sensitive
    LPC_GPIO_PIN_INT->SIENF = (1 << 1) | (1 << 2); // Enable low edge PININT interrupts

    /* Enable interrupt in the NVIC */
    NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
    NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
    NVIC_SetPriority(PIN_INT1_IRQn, 0);
    NVIC_SetPriority(PIN_INT2_IRQn, 0);
    NVIC_EnableIRQ(PIN_INT1_IRQn);
    NVIC_EnableIRQ(PIN_INT2_IRQn);
}

extern "C" {

void PIN_INT1_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 1);

    ButtonEvent btn;
    btn.pin = 1;
    btn.tickCount = xTaskGetTickCountFromISR();
    xQueueSendToBackFromISR(qBtn, &btn, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

void PIN_INT2_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 2);

    ButtonEvent btn;
    btn.pin = 2;
    btn.tickCount = xTaskGetTickCountFromISR();
    xQueueSendToBackFromISR(qBtn, &btn, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

/* the following is required if runtime statistics are to be collected */
void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
