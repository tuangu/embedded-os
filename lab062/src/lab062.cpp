#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <string.h>
#include <stdlib.h>

#include "common.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "user_vcom.h"

#define TO_RIGHT 0
#define TO_LEFT 1

// RIT
volatile uint32_t RIT_count;
xSemaphoreHandle sbRIT;
volatile bool step = false;
volatile bool dir;

// Command queue
xQueueHandle cmdQueue;
xSemaphoreHandle sbExecute;

void RIT_start(int count, int us);

void setupHardware();

Command* parseCmd(char *buffer);

void vReceiveTask(void *vParameters);
void vExecuteTask(void *vParameters);

int main(void) {

    setupHardware();

    sbRIT = xSemaphoreCreateBinary();
    sbExecute = xSemaphoreCreateBinary();
    cmdQueue = xQueueCreate(10, sizeof(Command *));

    xTaskCreate(vReceiveTask, "Receive Task", configMINIMAL_STACK_SIZE * 2,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vExecuteTask, "Execute Task", configMINIMAL_STACK_SIZE * 2,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(cdc_task, "CDC", configMINIMAL_STACK_SIZE * 2, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    // initialize RIT (= enable clocking etc.)
    Chip_RIT_Init(LPC_RITIMER);

    // set the priority level of the interrupt
    // The level must be equal or lower than the maximum priority specified in FreeRTOS config
    // Note that in a Cortex-M3 a higher number indicates lower interrupt priority
    NVIC_SetPriority(RITIMER_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1);

    // step pin of stepper motor
    Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 24, IOCON_MODE_INACT | IOCON_DIGMODE_EN);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 24);

    // direction pin of stepper motor
    Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 0, IOCON_MODE_INACT | IOCON_DIGMODE_EN);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 1, 0);

    // limit switch 1, input - pullup - invert true
    Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 27, IOCON_DIGMODE_EN | IOCON_INV_EN | IOCON_MODE_PULLUP);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, 0, 27);

    // limit switch 2, input - pullup - invert true
    Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 28, IOCON_DIGMODE_EN | IOCON_INV_EN | IOCON_MODE_PULLUP);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, 0, 28);
}
/*
 * Execute command from queue
 *  left <count> - runs <count> steps to left, <count> is an integer
 *  right <count> - runs <count> steps to right, <count> is an integer
 *  pps <count> - sets the number of driving pulses per second
*/
void vExecuteTask(void *vParameters) {

    /* Drive the belt connector block to the middle of the rail */
    {
        int pps = 200;
        int maxCount = 9999;

        dir = TO_LEFT;
        Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, TO_LEFT);
        RIT_start(2*maxCount, (1000000 / (2 * pps)));

        dir = TO_RIGHT;
        Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, TO_RIGHT);
        RIT_start(2*maxCount, (1000000 / (2 * pps)));
        int distance = maxCount - RIT_count / 2;

        dir = TO_LEFT;
        Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, TO_LEFT);
        RIT_start(2 * (distance / 2), (1000000 / (2 * pps)));
    }

    Command *cmd;
    bool isRunning = false;
    int pps = 400;

    while (1) {
        if (xSemaphoreTake(sbExecute, 0) == pdPASS)
            isRunning = true;

        if (isRunning) {
            if (xQueueReceive(cmdQueue, &cmd, 0) == pdPASS) {
                if (cmd->action == Command::stop) {
                    isRunning = false;
                } else if (cmd->action == Command::pps)
                    pps = cmd->value;
                else {
                    if (cmd->action == Command::left) {
                        dir = TO_LEFT;
                        Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, TO_LEFT);
                    } else if (cmd->action == Command::right) {
                        dir = TO_RIGHT;
                        Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, TO_RIGHT);
                    }
                    int count = cmd->value;

                    // start RIT
                    RIT_start(2*count, 1000000 / (2 * pps));
                }

                delete cmd;
            } else
                isRunning = false;
        }
    }
}

/*
 * Read commands from serial port and add them to queue.
 */
void vReceiveTask(void *vParameters) {

    while (1) {
        // read char into buffer from serial port
        char buffer[RCV_BUFSIZE] = "";
        int idx = 0;
        while (idx < 15) {
            int len = USB_receive((uint8_t *) (buffer + idx), RCV_BUFSIZE - idx);

            /* Find the first occurrence of '\r' */
            char *pos = strstr((buffer + idx), "\r");
            if (pos != NULL) {
                USB_send((uint8_t *) (buffer + idx), pos - (buffer + idx) + 1);
                USB_send((uint8_t *) "\n", 1);
                break;
            } else {
                USB_send((uint8_t *) (buffer + idx), len);
            }

            idx += len;
        }

        // parse command
        Command *cmd = parseCmd(buffer);

        if (cmd->action == Command::invalid) {
            delete cmd;
            continue;
        } else if (cmd->action == Command::go) {
            xSemaphoreGive(sbExecute);
            delete cmd;
        } else if (cmd->action == Command::stop) {
            BaseType_t status = xQueueSendToFront(cmdQueue, &cmd, 0);

            if (status == errQUEUE_FULL) {
                char err[] = "Error: command queue full";
                USB_send((uint8_t *) err, strlen(err));
            } else
                xSemaphoreGive(sbExecute);
        } else {
            BaseType_t status = xQueueSendToBack(cmdQueue, &cmd, 0);

            if (status == errQUEUE_FULL) {
                char err[] = "Error: command queue full";
                USB_send((uint8_t *) err, strlen(err));
            }
        }

    }
}

Command* parseCmd(char *buffer) {
    Command *ret = new Command();

    if (strncmp(buffer, "left", 4) == 0) {
        ret->action = Command::left;
        ret->value = strtol(buffer+4, NULL, 10);
    } else if (strncmp(buffer, "right", 5) == 0) {
        ret->action = Command::right;
        ret->value = strtol(buffer+5, NULL, 10);
    } else if (strncmp(buffer, "pps", 3) == 0) {
        ret->action = Command::pps;
        ret->value = strtol(buffer+3, NULL, 10);
    } else if (strncmp(buffer, "stop", 4) == 0) {
        ret->action = Command::stop;
    } else if (strncmp(buffer, "go", 2) == 0) {
        ret->action = Command::go;
    } else {
        ret->action = Command::invalid;
    }

    return ret;
}

void RIT_start(int count, int us) {
    uint64_t cmp_value;

    // Determine approximate compare value based on clock rate and passed interval
    cmp_value = (uint64_t) Chip_Clock_GetSystemClockRate() * (uint64_t) us / 1000000;

    // disable timer during configuration
    Chip_RIT_Disable(LPC_RITIMER);

    RIT_count = count;
    // enable automatic clear on when compare value==timer value
    // this makes interrupts trigger periodically
    Chip_RIT_EnableCompClear(LPC_RITIMER);

    // reset the counter
    Chip_RIT_SetCounter(LPC_RITIMER, 0);
    Chip_RIT_SetCompareValue(LPC_RITIMER, cmp_value);

    // start counting
    Chip_RIT_Enable(LPC_RITIMER);

    // Enable the interrupt signal in NVIC (the interrupt controller)
    NVIC_EnableIRQ(RITIMER_IRQn);

    // wait for ISR to tell that we're done
    if (xSemaphoreTake(sbRIT, portMAX_DELAY) == pdTRUE) {
        // Disable the interrupt signal in NVIC (the interrupt controller)
        NVIC_DisableIRQ(RITIMER_IRQn);
    } else {
        // unexpected error
    }
}


extern "C" {

void RIT_IRQHandler(void) {
    // This used to check if a context switch is required
    portBASE_TYPE xHigherPriorityWoken = pdFALSE;

    // Tell timer that we have processed the interrupt.
    // Timer then removes the IRQ until next match occurs
    Chip_RIT_ClearIntStatus(LPC_RITIMER); // clear IRQ flag

    if (RIT_count > 0) {
        RIT_count--;

        bool lm1 = Chip_GPIO_GetPinState(LPC_GPIO, 0, 27) && (dir == TO_LEFT);
        bool lm2 = Chip_GPIO_GetPinState(LPC_GPIO, 0, 28) && (dir == TO_RIGHT);

        if (lm1 || lm2) {
            // Hit limit switches

            // Disable timer
            Chip_RIT_Disable(LPC_RITIMER);

            // Give semaphore
            xSemaphoreGiveFromISR(sbRIT, &xHigherPriorityWoken);
        } else {
            // drive the stepper motor
            Chip_GPIO_SetPinState(LPC_GPIO, 0, 24, step);
            step = !step;

            if ((RIT_count % 100) == 0) {
                Board_LED_Toggle(2);
            }
        }

    } else {
        Chip_RIT_Disable(LPC_RITIMER); // disable timer
        // Give semaphore and set context switch flag if a higher priority task was woken up
        xSemaphoreGiveFromISR(sbRIT, &xHigherPriorityWoken);
    }

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

/* the following is required if runtime statistics are to be collected */
void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
