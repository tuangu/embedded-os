#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "stdlib.h"
#include <string.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

void setupHardware();
void tLedCallback(TimerHandle_t timer);
void tMonitorCallback(TimerHandle_t timer);

void vTask1(void *vParameters);

SerialLog serial;
QueueHandle_t qMonitor;
QueueHandle_t qLed;
TimerHandle_t tLed;
TimerHandle_t tMonitor;

int main(void) {

    setupHardware();

    qMonitor = xQueueCreate(1, sizeof(int));
    qLed = xQueueCreate(1, sizeof(TickType_t));

    tLed = xTimerCreate("LED Timer", 5 * configTICK_RATE_HZ,
                        pdTRUE, 0, tLedCallback);

    tMonitor = xTimerCreate("Monitoring Timer", 10 * configTICK_RATE_HZ,
                            pdFALSE, 0, tMonitorCallback);

    xTaskCreate(vTask1, "Task 1", configMINIMAL_STACK_SIZE * 4,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vTask1(void *vParameters) {

    xTimerStart(tLed, portMAX_DELAY);
    xTimerStart(tMonitor, portMAX_DELAY);

    int recvMonitor = 1;
    int recvLed;

    while (1) {
        const int BUFFER_SIZE = 16;
        char buffer[BUFFER_SIZE];
        int i = 0;
        while (i < BUFFER_SIZE) {
            xQueueReceive(qMonitor, &recvMonitor, 0);
            if (recvMonitor != 1) {
                // No char received in 30 seconds

                buffer[0] = '\0'; // reset buffer
                // Reset timer
                recvMonitor = 1;
                xTimerReset(tMonitor, portMAX_DELAY);
                break;
            }

            int ch = serial.read();

            if (ch != EOF) {
                serial.write((char *) &ch);

                // Update buffer
                *(buffer + i) = ch;
                i++;

                if (ch == '\r') {
                    serial.write((char *) "\n");
                    break;
                }

                xTimerReset(tMonitor, portMAX_DELAY);
            }
        }

        if (strncmp(buffer, "help", 4) == 0) {
            // Print usage
            char str[] = "\t interval <number> - set toogle interval in seconds \r\n\t "
                         "time - print the number of seconds since the last toggle\r\n";

            serial.write(str);

        } else if (strncmp(buffer, "time", 4) == 0) {
            // Print number of seconds since the last toggle
            BaseType_t status = xQueuePeek(qLed, &recvLed, 0);

            if (status == errQUEUE_EMPTY)
                recvLed = 0;

            TickType_t now = xTaskGetTickCount();
            float seconds = (now - recvLed) / 1000.0;

            // char str[16];
            snprintf(buffer, 16, "%.1f\r\n", seconds);
            serial.write(buffer);

        } else if (strncmp(buffer, "interval", 8) == 0) {
            // Update toggle interval
            int interval = strtol(buffer + 8, NULL, 10);

            xTimerChangePeriod(tLed, interval * configTICK_RATE_HZ, portMAX_DELAY);
            xTimerReset(tLed, portMAX_DELAY);
        }
    }
}

void tLedCallback(TimerHandle_t timer) {
    Board_LED_Toggle(1);

    TickType_t now = xTaskGetTickCount();
    xQueueOverwrite(qLed, &now);
}

void tMonitorCallback(TimerHandle_t timer) {
    char str[] = "[Inactive]\r\n";
    serial.write(str);

    int n = 0;
    xQueueOverwrite(qMonitor, &n);
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
