/*
 * common.h
 *
 *  Created on: Sep 11, 2017
 *      Author: tuanngu
 */

#ifndef COMMON_H_
#define COMMON_H_

struct Command {
    enum actionType {
        left,
        right,
        pps,
        go,
        stop,
        invalid
    };

    actionType action;
    int value;
};



#endif /* COMMON_H_ */
