#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdlib.h>
#include <time.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

#define BUTTON_BIT(c) (1 << (c))

void setupHardware();
void Button_Setup();

void vTask1(void *vParameters);
void vTask2(void *vParameters);
void vTask3(void *vParameters);
void vWatchdogTask(void *vParameters);

SerialLog serial;
EventGroupHandle_t egBtn;

int main(void) {

    setupHardware();

    egBtn = xEventGroupCreate();

    xTaskCreate(vTask1, "Task 1", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask2, "Task 2", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask3, "Task 3", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vWatchdogTask, "Watchdog Task", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void vTask1(void *vParameters) {
    DigitalIoPin btn(0, 17, DigitalIoPin::pullup, true);

    bool prevReading;

    while (1) {
        bool btnReading = btn.read();

        if (!btnReading && (btnReading != prevReading))
            xEventGroupSetBits(egBtn, BUTTON_BIT(1));

        prevReading = btnReading;
    }
}

void vTask2(void *vParameters) {
    DigitalIoPin btn(1, 11, DigitalIoPin::pullup, true);

    bool prevReading;

    while (1) {
        bool btnReading = btn.read();

        if (!btnReading && (btnReading != prevReading))
            xEventGroupSetBits(egBtn, BUTTON_BIT(2));

        prevReading = btnReading;
    }
}

void vTask3(void *vParameters) {
    DigitalIoPin btn(1, 9, DigitalIoPin::pullup, true);

    bool prevReading;

    while (1) {
        bool btnReading = btn.read();

        if (!btnReading && (btnReading != prevReading))
            xEventGroupSetBits(egBtn, BUTTON_BIT(3));

        prevReading = btnReading;
    }
}

void vWatchdogTask(void *vParameters) {

    EventBits_t eventValue;
    TickType_t lastTick = 0;
    uint8_t failCount[3] = {0};

    const EventBits_t bitToWait = BUTTON_BIT(1) | BUTTON_BIT(2) | BUTTON_BIT(3);
    const TickType_t tickToWait = configTICK_RATE_HZ * 30;

    while (1) {
        eventValue = xEventGroupWaitBits(egBtn, bitToWait, pdTRUE, pdTRUE, tickToWait);

        char buffer[32];

        if (eventValue == bitToWait) {
            TickType_t currTick = xTaskGetTickCount();

            snprintf(buffer, 32, "OK %lu\r\n", currTick - lastTick);
            serial.write(buffer);

            lastTick = currTick;
            for (uint8_t i = 0; i < 3; i++)
                failCount[i] = 0;
        } else {
            char message[] = "Fail \r\n";
            serial.write(message);

            if ((eventValue & BUTTON_BIT(1)) != 0) {
                failCount[0] = 0;
            } else {
                ++failCount[0];
                snprintf(buffer, 32, "     Task 1 %lu\r\n", failCount[0]*tickToWait);
                serial.write(buffer);
            }

            if ((eventValue & BUTTON_BIT(2)) != 0) {
                failCount[1] = 0;
            } else {
                ++failCount[1];
                snprintf(buffer, 32, "     Task 2 %lu\r\n", failCount[1]*tickToWait);
                serial.write(buffer);
            }

            if ((eventValue & BUTTON_BIT(3)) != 0) {
                failCount[2] = 0;
            } else {
                ++failCount[2];
                snprintf(buffer, 32, "     Task 3 %lu\r\n", failCount[2]*tickToWait);
                serial.write(buffer);
            }
        }

        xEventGroupClearBits(egBtn, bitToWait);
    }
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

extern "C" {

/* the following is required if runtime statistics are to be collected */
void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
