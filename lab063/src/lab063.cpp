#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <string.h>
#include <stdlib.h>

#include "common.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "user_vcom.h"

#define TO_RIGHT 0
#define TO_LEFT 1

// RIT
volatile uint32_t RIT_count;
xSemaphoreHandle sbRIT;
volatile bool step = false;
volatile bool dir;

volatile bool isCalibrating;
xSemaphoreHandle sbSpeedTest;

void RIT_start(int count, int us);

void setupHardware();

void vSpeedTestTask(void *vParameters);
void vLedTask(void *vParameters);

int main(void) {

    setupHardware();

    sbRIT = xSemaphoreCreateBinary();
    sbSpeedTest = xSemaphoreCreateBinary();

    xTaskCreate(vSpeedTestTask, "Speed Test Task", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vLedTask, "LED Task", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(cdc_task, "CDC", configMINIMAL_STACK_SIZE * 2, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    // initialize RIT (= enable clocking etc.)
    Chip_RIT_Init(LPC_RITIMER);

    // set the priority level of the interrupt
    // The level must be equal or lower than the maximum priority specified in FreeRTOS config
    // Note that in a Cortex-M3 a higher number indicates lower interrupt priority
    NVIC_SetPriority(RITIMER_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1);

    // step pin of stepper motor
    Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 24, IOCON_MODE_INACT | IOCON_DIGMODE_EN);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 0, 24);

    // direction pin of stepper motor
    Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 0, IOCON_MODE_INACT | IOCON_DIGMODE_EN);
    Chip_GPIO_SetPinDIROutput(LPC_GPIO, 1, 0);

    // limit switch left, input - pullup - invert true
    Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 27, IOCON_DIGMODE_EN | IOCON_INV_EN | IOCON_MODE_PULLUP);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, 0, 27);

    // limit switch right, input - pullup - invert true
    Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 28, IOCON_DIGMODE_EN | IOCON_INV_EN | IOCON_MODE_PULLUP);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, 0, 28);
}

void vLedTask(void *vParameters) {

    while (1) {
        if (Chip_GPIO_GetPinState(LPC_GPIO, 0, 27)) {
            Board_LED_Set(0, true);
            vTaskDelay(configTICK_RATE_HZ / 2);
        } else if (Chip_GPIO_GetPinState(LPC_GPIO, 0, 28)) {
            Board_LED_Set(1, true);
            vTaskDelay(configTICK_RATE_HZ / 2);
        } else {
            Board_LED_Set(0, false);
            Board_LED_Set(1, false);
        }
    }
}

void vSpeedTestTask(void *vParameters) {
    int avgDistance = 0;

    /* Determine the number of steps between the switches */
    {
        isCalibrating = true;
        int pps = 400;
        int us = 1000000 / (2 * pps);
        int maxCount = 9999;

        char buffer[20];
        int distance[5] = {0};

        dir = TO_LEFT;
        Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, TO_LEFT);
        RIT_start(2*maxCount, us);

        USB_send((uint8_t *) "Start calibrating\r\n", 19);

        for (int i = 0; i < 5; i++) {
            // Set direction
            dir = !dir;
            Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, dir);

            // Start RIT
            RIT_start(2*maxCount, us);

            distance[i] = 2*maxCount - RIT_count;
            avgDistance += distance[i];

            snprintf(buffer, 20, "Distance = %d\r\n", distance[i]);
            USB_send((uint8_t *) buffer, strlen(buffer));
        }

        avgDistance = avgDistance / 5 / 2;
    }

    isCalibrating = false;
    int stop = false;

    int pps_start = 1000;
    int pps_target = 1000;
    const uint8_t INTERVAL = 100;
    const int ACCELERATION_STEP = avgDistance / 3 / INTERVAL; // 25% of total steps / Interval
    const int DECELERATION_STEP = ACCELERATION_STEP;
    const int RUNNING_STEP = avgDistance - 2 * ACCELERATION_STEP * INTERVAL + 10;

    USB_send((uint8_t *) "Speed test starts\r\n", 19);

    while (1) {
        if (!stop) {

            /* Change direction */
            dir = (bool) !dir;
            Chip_GPIO_SetPinState(LPC_GPIO, 1, 0, dir);

            int pps_current = pps_start;
            pps_target += 100;
            int delta_pps = (pps_target - pps_start) / INTERVAL;

            /* Accelerate the stepper motor */
            for (int i = 0; i < INTERVAL; i++) {
                pps_current = pps_start + (i + 1) * delta_pps;
                RIT_start(2 * ACCELERATION_STEP, (1000000 / (2 * pps_target)));
                while (xSemaphoreTake(sbSpeedTest, portMAX_DELAY) != pdPASS);
            }

            /* Running at target speed */
            RIT_start(2 * RUNNING_STEP, (1000000 / (2 * pps_current)));
            while (xSemaphoreTake(sbSpeedTest, portMAX_DELAY) != pdPASS);

            /* Decelerate the stepper motor */
            for (int i = 0; i < INTERVAL; i++) {
                pps_current = pps_target - (i + 1) * delta_pps;
                RIT_start(2 * DECELERATION_STEP, (1000000 / (2 * pps_current)));
                while (xSemaphoreTake(sbSpeedTest, portMAX_DELAY) != pdPASS);
            }

            vTaskDelay(100);

            /* Check whether the limit switch is not hit after the calculated number of pulses has been given */
            bool lm1 = Chip_GPIO_GetPinState(LPC_GPIO, 0, 27) && (dir == TO_LEFT);
            bool lm2 = Chip_GPIO_GetPinState(LPC_GPIO, 0, 28) && (dir == TO_RIGHT);
            if (lm1 || lm2) {
                char buffer[36];
                snprintf(buffer, 36, "[Speed Test] pps = %d\r\n", pps_target);
                USB_send((uint8_t *) buffer, strlen(buffer));
            } else {
                stop = true;
                USB_send((uint8_t *) "Speed test ends\r\n", 17);

                char buffer[36];
                snprintf(buffer, 36, "Max pps = %d\r\n", pps_target - 100);
                USB_send((uint8_t *) buffer, strlen(buffer));

            }
        }
    }
}

void RIT_start(int count, int us) {
    uint64_t cmp_value;

    // Determine approximate compare value based on clock rate and passed interval
    cmp_value = (uint64_t) Chip_Clock_GetSystemClockRate() * (uint64_t) us / 1000000;

    // disable timer during configuration
    Chip_RIT_Disable(LPC_RITIMER);

    RIT_count = count;
    // enable automatic clear on when compare value==timer value
    // this makes interrupts trigger periodically
    Chip_RIT_EnableCompClear(LPC_RITIMER);

    // reset the counter
    Chip_RIT_SetCounter(LPC_RITIMER, 0);
    Chip_RIT_SetCompareValue(LPC_RITIMER, cmp_value);

    // start counting
    Chip_RIT_Enable(LPC_RITIMER);

    // Enable the interrupt signal in NVIC (the interrupt controller)
    NVIC_EnableIRQ(RITIMER_IRQn);

    // wait for ISR to tell that we're done
    if (xSemaphoreTake(sbRIT, portMAX_DELAY) == pdTRUE) {
        // Disable the interrupt signal in NVIC (the interrupt controller)
        NVIC_DisableIRQ(RITIMER_IRQn);
    } else {
        // unexpected error
    }
}


extern "C" {

void RIT_IRQHandler(void) {
    // This used to check if a context switch is required
    portBASE_TYPE xHigherPriorityWoken = pdFALSE;

    // Tell timer that we have processed the interrupt.
    // Timer then removes the IRQ until next match occurs
    Chip_RIT_ClearIntStatus(LPC_RITIMER); // clear IRQ flag

    if (RIT_count > 0) {
        RIT_count--;

        if (isCalibrating) {
            bool lm1 = Chip_GPIO_GetPinState(LPC_GPIO, 0, 27) && (dir == TO_LEFT);
            bool lm2 = Chip_GPIO_GetPinState(LPC_GPIO, 0, 28) && (dir == TO_RIGHT);

            if (lm1 || lm2) {
                // Hit limit switches

                // Disable timer
                Chip_RIT_Disable(LPC_RITIMER);

                // Give semaphore
                xSemaphoreGiveFromISR(sbRIT, &xHigherPriorityWoken);
            } else {
                // drive the stepper motor
                Chip_GPIO_SetPinState(LPC_GPIO, 0, 24, step);
                step = !step;

                if ((RIT_count % 100) == 0) {
                    Board_LED_Toggle(2);
                }
            }
        } else {
            // drive the stepper motor
            Chip_GPIO_SetPinState(LPC_GPIO, 0, 24, step);
            step = !step;

            if ((RIT_count % 100) == 0) {
                Board_LED_Toggle(2);
            }
        }


    } else {
        Chip_RIT_Disable(LPC_RITIMER); // disable timer
        // Give semaphore and set context switch flag if a higher priority task was woken up
        xSemaphoreGiveFromISR(sbRIT, &xHigherPriorityWoken);

        if (!isCalibrating)
            xSemaphoreGiveFromISR(sbSpeedTest, &xHigherPriorityWoken);
    }

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

/* the following is required if runtime statistics are to be collected */
void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
