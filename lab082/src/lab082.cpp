#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ButtonEvent.h"
#include "SerialLog.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
// #include "user_vcom.h"

#define BUTTON_1_PORT 0
#define BUTTON_1_PIN 17
#define BUTTON_2_PORT 1
#define BUTTON_2_PIN 11
#define BUTTON_3_PORT 1
#define BUTTON_3_PIN 9

void vSetupHardware();
void Button_Setup();

void vSerialWriteTask(void *vParameters);
void vSerialReadTask(void *vParameters);

xQueueHandle qBtn;
xQueueHandle mFilter;
SerialLog serial;

int main(void) {

    vSetupHardware();

    qBtn = xQueueCreate(10, sizeof(ButtonEvent));

    mFilter = xQueueCreate(1, sizeof(long));

    xTaskCreate(vSerialWriteTask, "Serial Write Task", configMINIMAL_STACK_SIZE * 4,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vSerialReadTask, "Serial Read Task", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

//    xTaskCreate(cdc_task, "CDC", configMINIMAL_STACK_SIZE * 3, NULL,
//                (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    return 0;
}

void vSerialWriteTask(void *vParameters) {

    ButtonEvent prevRecv;
    ButtonEvent recv;

    long filter = 50;

//    {
//        xQueueReceive(qBtn, &recv, portMAX_DELAY);
//        prevRecv = recv;
//    }

    while (1) {
        xQueueReceive(qBtn, &recv, portMAX_DELAY);

        xQueuePeek(mFilter, &filter, 0);
        long delta = (recv.tickCount - prevRecv.tickCount) * 1000 / configTICK_RATE_HZ;
        if (delta > filter) {
            char buffer[24];
            int len;

            if (delta > 1000) {
                float deltaInSecond = delta / 1000.0;
                len = snprintf(buffer, 24, "%.2f s Button %d\r\n", deltaInSecond, recv.pin);
            } else {
                len = snprintf(buffer, 24, "%ld ms Button %d\r\n", delta, recv.pin);
            }

            serial.write(buffer);
            // USB_send((uint8_t *) buffer, len);
            prevRecv = recv;
        }
    }
}

void vSerialReadTask(void *vParameters) {

    while (1) {
//        char buffer[RCV_BUFSIZE] = "";
//        int idx = 0;
//        while (idx < RCV_BUFSIZE) {
//            int len = USB_receive((uint8_t *) (buffer + idx), RCV_BUFSIZE);
//
//            /* Find the first occurrence of '\r' */
//            char *pos = strstr((buffer + idx), "\r");
//            if (pos != NULL) {
//                USB_send((uint8_t *) (buffer + idx), pos - (buffer + idx) + 1);
//                USB_send((uint8_t *) "\n", 1);
//                break;
//            } else {
//                USB_send((uint8_t *) (buffer + idx), len);
//            }
//
//            idx += len;
//        }

        const int BUFFER_SIZE = 16;
        char buffer[BUFFER_SIZE];
        int i = 0;
        while (i < BUFFER_SIZE) {
            int ch = serial.read();

            if (ch != EOF) {
                serial.write((char *) &ch);
                if (ch == '\r') {
                    serial.write((char *) "\n");
                    break;
                }

                *(buffer + i) = ch;
                i++;
            }
        }

        if (strncmp(buffer, "filter ", 7) == 0) {
            long filter = strtol(buffer + 7, NULL, 10);

            if (filter > 0) {
                xQueueOverwrite(mFilter, &filter);

                char buffer[24];
                snprintf(buffer, 24, "Filter time: %ld\r\n", filter);
                // USB_send((uint8_t *) buffer, strlen(buffer));
                serial.write(buffer);
            }
        }
    }
}

void vSetupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Button_Setup();
}

void Button_Setup() {
    /* Set pin back to GPIO */
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_1_PORT, BUTTON_1_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_2_PORT, BUTTON_2_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_3_PORT, BUTTON_3_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);

    /* Configure GPIO pin as input */
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_1_PORT, BUTTON_1_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_2_PORT, BUTTON_2_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_3_PORT, BUTTON_3_PIN);

    /* Enable PININT clock */
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_PININT);

    /* Reset the PININT block */
    Chip_SYSCTL_PeriphReset(RESET_PININT);

    /* Configure interrupt channel for the GPIO pin in INMUX block */
    Chip_INMUX_PinIntSel(1, BUTTON_1_PORT, BUTTON_1_PIN);
    Chip_INMUX_PinIntSel(2, BUTTON_2_PORT, BUTTON_2_PIN);
    Chip_INMUX_PinIntSel(3, BUTTON_3_PORT, BUTTON_3_PIN);

    /* Configure channel interrupt as edge sensitive and falling edge interrupt */
    LPC_GPIO_PIN_INT->IST = (1 << 1) | (1 << 2) | (1 << 3); // Clear interrupt status
    LPC_GPIO_PIN_INT->ISEL &= ~(1 << 1) | ~(1 << 2) | ~(1 << 3); // Configure the pins as edge sensitive
    LPC_GPIO_PIN_INT->SIENF = (1 << 1) | (1 << 2) | (1 << 3); // Enable low edge PININT interrupts

    /* Enable interrupt in the NVIC */
    NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
    NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
    NVIC_ClearPendingIRQ(PIN_INT3_IRQn);
    NVIC_SetPriority(PIN_INT1_IRQn, 0);
    NVIC_SetPriority(PIN_INT2_IRQn, 0);
    NVIC_SetPriority(PIN_INT3_IRQn, 0);
    NVIC_EnableIRQ(PIN_INT1_IRQn);
    NVIC_EnableIRQ(PIN_INT2_IRQn);
    NVIC_EnableIRQ(PIN_INT3_IRQn);
}

extern "C" {

void PIN_INT1_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 1);

    ButtonEvent btn1;
    btn1.pin = 1;
    btn1.tickCount = xTaskGetTickCountFromISR();
    xQueueSendToBackFromISR(qBtn, &btn1, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

void PIN_INT2_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 2);

    ButtonEvent btn2;
    btn2.pin = 2;
    btn2.tickCount = xTaskGetTickCountFromISR();
    xQueueSendToBackFromISR(qBtn, &btn2, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

void PIN_INT3_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 3);

    ButtonEvent btn3;
    btn3.pin = 3;
    btn3.tickCount = xTaskGetTickCountFromISR();
    xQueueSendToBackFromISR(qBtn, &btn3, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

/* the following is required if runtime statistics are to be collected */
void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
