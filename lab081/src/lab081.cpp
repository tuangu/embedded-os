#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
#include <stdio.h>

#include "SerialLog.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define BUTTON_1_PORT 0
#define BUTTON_1_PIN 17
#define BUTTON_2_PORT 1
#define BUTTON_2_PIN 11
#define BUTTON_3_PORT 1
#define BUTTON_3_PIN 9

void vSetupHardware();
void Button_Setup();

void vButtonTask(void *vParameters);

xQueueHandle qBtn;

int main(void) {

    vSetupHardware();

    qBtn = xQueueCreate(10, sizeof(uint8_t));

    xTaskCreate(vButtonTask, "Button Task", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    return 0;
}

void vButtonTask(void *vParameters) {
    SerialLog serial;

    uint8_t recvBuf[24] = {0};
    int idx = 0;
    uint8_t recv;

    while (1) {
        xQueueReceive(qBtn, &recv, portMAX_DELAY);
        if (idx == 0) {
            recvBuf[idx] = recv;
            idx++;
        } else {
            if (recv == recvBuf[idx-1])
                recvBuf[idx++] = recv;
            else if (recv != recvBuf[idx-1]) {
                char serialBuf[32];
                snprintf(serialBuf, 32, "Button %d pressed %d times.\r\n", recvBuf[idx-1], idx);
                serial.write(serialBuf);

                idx = 0;
                recvBuf[idx++] = recv;
            }
        }
    }
}

void vSetupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Button_Setup();
}

void Button_Setup() {
    /* Set pin back to GPIO */
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_1_PORT, BUTTON_1_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_2_PORT, BUTTON_2_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);
    Chip_IOCON_PinMuxSet(LPC_IOCON, BUTTON_3_PORT, BUTTON_3_PIN,
                         IOCON_DIGMODE_EN | IOCON_MODE_INACT | IOCON_MODE_PULLUP);

    /* Configure GPIO pin as input */
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_1_PORT, BUTTON_1_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_2_PORT, BUTTON_2_PIN);
    Chip_GPIO_SetPinDIRInput(LPC_GPIO, BUTTON_3_PORT, BUTTON_3_PIN);

    /* Enable PININT clock */
    Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_PININT);

    /* Reset the PININT block */
    Chip_SYSCTL_PeriphReset(RESET_PININT);

    /* Configure interrupt channel for the GPIO pin in INMUX block */
    Chip_INMUX_PinIntSel(1, BUTTON_1_PORT, BUTTON_1_PIN);
    Chip_INMUX_PinIntSel(2, BUTTON_2_PORT, BUTTON_2_PIN);
    Chip_INMUX_PinIntSel(3, BUTTON_3_PORT, BUTTON_3_PIN);

    /* Configure channel interrupt as edge sensitive and falling edge interrupt */
    LPC_GPIO_PIN_INT->IST = (1 << 1) | (1 << 2) | (1 << 3); // Clear interrupt status
    LPC_GPIO_PIN_INT->ISEL &= ~(1 << 1) | ~(1 << 2) | ~(1 << 3); // Configure the pins as edge sensitive
    LPC_GPIO_PIN_INT->SIENF = (1 << 1) | (1 << 2) | (1 << 3); // Enable low edge PININT interrupts

    /* Enable interrupt in the NVIC */
    NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
    NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
    NVIC_ClearPendingIRQ(PIN_INT3_IRQn);
    NVIC_SetPriority(PIN_INT1_IRQn, 0);
    NVIC_SetPriority(PIN_INT2_IRQn, 0);
    NVIC_SetPriority(PIN_INT3_IRQn, 0);
    NVIC_EnableIRQ(PIN_INT1_IRQn);
    NVIC_EnableIRQ(PIN_INT2_IRQn);
    NVIC_EnableIRQ(PIN_INT3_IRQn);
}

extern "C" {

void PIN_INT1_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 1);

    uint8_t n = 1;
    xQueueSendToBackFromISR(qBtn, &n, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

void PIN_INT2_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 2);

    uint8_t n = 2;
    xQueueSendToBackFromISR(qBtn, &n, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

void PIN_INT3_IRQHandler(void) {
    // This used to check if a context switch is required
    BaseType_t xHigherPriorityWoken = pdFALSE;

    // Clear interrupt status
    LPC_GPIO_PIN_INT->IST = (1 << 3);

    uint8_t n = 3;
    xQueueSendToBackFromISR(qBtn, &n, &xHigherPriorityWoken);

    // End the ISR and (possibly) do a context switch
    portEND_SWITCHING_ISR(xHigherPriorityWoken);
}

/* the following is required if runtime statistics are to be collected */
void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
