#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include <ctype.h>

#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define LED_RED 0
#define LED_GREEN 1
#define LED_BLUE 2

void setupHardware();
void vLimitSwitchTask(void *vParameters);
void vMotorTask(void *vParameters);

SemaphoreHandle_t motorSemphr;

int main(void) {

    setupHardware();

    motorSemphr = xSemaphoreCreateBinary();

    xTaskCreate(vLimitSwitchTask, "Limit Switch Task", configMINIMAL_STACK_SIZE,
            NULL, (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

    xTaskCreate(vMotorTask, "Motor Task", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(LED_BLUE, false);
    Board_LED_Set(LED_GREEN, false);
    Board_LED_Set(LED_RED, false);
}

/*
 *  1. When task starts it reads limit switches and waits until both limit switches are open. The blue led
 *	blinks during waiting.
 *	2. Then task signals “go” using binary semaphore
 *	3. Then task enters a loop where it reads limit switches and switches red led on if limit switch 1 is
 *	closed and green led on if limit switch 2 is closed.
 */
void vLimitSwitchTask(void *vParameters) {
    DigitalIoPin lm1(0, 27, DigitalIoPin::pullup, false);
    DigitalIoPin lm2(0, 28, DigitalIoPin::pullup, false);

    TickType_t tickToBlink = 100;
    bool ledState = true;
    bool isRunning = false;

    while (1) {
        bool lm1Reading = lm1.read();
        bool lm2Reading = lm2.read();

        if (isRunning) {
            if (!lm1Reading && !lm2Reading) {
                isRunning = false;
                Board_LED_Set(LED_GREEN, false);
                Board_LED_Set(LED_RED, false);
            } else if (!lm1Reading)
                Board_LED_Set(LED_RED, true);
            else if (!lm2Reading)
                Board_LED_Set(LED_GREEN, true);
            else {
                Board_LED_Set(LED_GREEN, false);
                Board_LED_Set(LED_RED, false);
            }
        } else {
            if (!lm1Reading && !lm2Reading) {
                // Blink Blue LED while waiting
                Board_LED_Set(LED_BLUE, ledState);
                ledState = !ledState;
                vTaskDelay(tickToBlink);
            } else {
                // Limit switches are open, notify vMotorTask by giving semaphore
                Board_LED_Set(LED_BLUE, false);
                isRunning = true;
                xSemaphoreGive(motorSemphr);
            }

        }
    }
}

/*
 * 	1. Task waits on the binary semaphore. When it gets the semaphore (“go” signal) the task continues
 * 	2. Task starts to run stepper motor in either direction and runs the motor at constant speed until one
 *	of the limit switches is closed. Then the motor direction is toggled. Note that you need to run a few
 *	steps in the opposite direction before the switch opens again or you need to know which limit
 *	switch is at each end and monitor only one switch per direction.
 *	3. If at any time after “go” both limit switches are closed the motor is stopped immediately and task
 *	waits for 5 seconds. Then the switches are checked again and running continues only if both
 *	switches are open.
 */
void vMotorTask(void *vParameters) {
    DigitalIoPin stepPin(0, 24, DigitalIoPin::output, false);
    DigitalIoPin directionPin(1, 0, DigitalIoPin::output, false);
    DigitalIoPin lm1(0, 27, DigitalIoPin::pullup, false);
    DigitalIoPin lm2(0, 28, DigitalIoPin::pullup, false);

    /*
     * Direction 0: toward limit switch 2
     * Direction 1: toward limit switch 1
     */
    bool direction = 0;
    bool step = 0;

    bool isRunning = false;

    while (1) {
        if (xSemaphoreTake(motorSemphr, 0) == pdTRUE)
            isRunning = true;

        if (isRunning) {
            stepPin.write(step);
            step = !step;
            vTaskDelay(1);

            bool lm1Reading = lm1.read();
            bool lm2Reading = lm2.read();

            // 2 limit switched are close
            if (!lm1Reading && !lm2Reading) {
                isRunning = false;
                vTaskDelay(configTICK_RATE_HZ * 5);
            }

            // Hit a limit switch
            if ((!direction && !lm2Reading) || (direction && !lm1Reading)) {
                direction = !direction;
                directionPin.write(direction);
            }
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
