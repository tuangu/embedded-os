#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "SerialLog.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define SERIAL_LIMIT 60

void setupHardware();

SemaphoreHandle_t countingSemaphore;
SerialLog logger;

void vSerialTask(void *vParameters);
void vOracleTask(void *vParameters);

int main(void) {

    setupHardware();

    // queue up to 10 events
    countingSemaphore = xSemaphoreCreateCounting(10, 0);

    xTaskCreate(vSerialTask, "Serial  Task", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vOracleTask, "Oracle Task", 128, NULL, (tskIDLE_PRIORITY + 1UL),
            (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vSerialTask(void *vParameters) {
    int index = 0;
    bool isQuestion = false;

    while (1) {
        int c = logger.read();
        if ((index < SERIAL_LIMIT) && (c != EOF) && (c != '\r')
                && (c != '\n')) {
            // reserve spaces for [You]
            if (index == 0)
                logger.write("      ");

            if (c == '?')
                isQuestion = true;

            // echo back received character
            logger.write((char *) &c);
            ++index;

        } else if ((c == '\r') || (c == '\n') || (index >= SERIAL_LIMIT)) {
            // deal with empty line
            if (index == 0) {
                logger.write("\r\n");
                continue;
            }

            // end of line
            logger.write("\r");
            logger.write("[You]");
            logger.write("\r\n");
            index = 0;

            // notify vOracleTask
            if (isQuestion)
                xSemaphoreGive(countingSemaphore);

            // clear flag
            isQuestion = false;
        }
    }
}

void vOracleTask(void *vParameters) {
    static char *answers[7] = { "After she left, I feel cold.\r\n",
            "She waits. War ended. No homecoming.\r\n",
            "Two yesterday. One today. None tomorrow.\r\n",
            "The sound of clocks and crickets.\r\n",
            "Where did this cow come from?\r\n",
            "Woke up, then took a rest.\r\n", "At least my dog loves me.\r\n", };

    while (1) {
        if (xSemaphoreTake(countingSemaphore, portMAX_DELAY) == pdPASS) {
            logger.write("\r\n[Oracle] Hmmm...\r\n");
            vTaskDelay(configTICK_RATE_HZ * 3);
            logger.write("\r\n[Oracle] ");
            logger.write(answers[(xTaskGetTickCount() % 7)]);
            logger.write("\r\n");
            vTaskDelay(configTICK_RATE_HZ * 2);
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
