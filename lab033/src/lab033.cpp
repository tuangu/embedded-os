#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include <ctype.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"
#include "ITM_write.h"
#include "DebugEvent.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define MAX_WORD_LENGTH 10
#define BUTTON_PRESSED 0

void setupHardware();
void vSerialTask(void *vParameters);
void vButtonTask(void *vParameters);
void vDebugPrintTask(void *vParameters);

QueueHandle_t debugMessageQueue;
SerialLog serial;
char *debugFormat[2] = { "Received %d characters at %d\n",
        "Button is pressed %d ticks at %d\n" };

int main(void) {

    setupHardware();

    debugMessageQueue = xQueueCreate(3, sizeof(debugEvent *));

    if (debugMessageQueue != NULL) {
        xTaskCreate(vSerialTask, "Serial Task", configMINIMAL_STACK_SIZE * 2,
                NULL, (tskIDLE_PRIORITY + 2), (TaskHandle_t *) NULL);

        xTaskCreate(vButtonTask, "Button Task", configMINIMAL_STACK_SIZE, NULL,
                (tskIDLE_PRIORITY + 2), (TaskHandle_t *) NULL);

        xTaskCreate(vDebugPrintTask, "Debug Print Task", (uint32_t ) 512, NULL,
                (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

        vQueueAddToRegistry(debugMessageQueue, "Debug Queue");
    }

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();
    ITM_init();

    Board_LED_Set(0, false);
}

void vSerialTask(void *vParameters) {
    int receivedChar;
    int idx = 0;

    TickType_t tickToWait = configTICK_RATE_HZ / 10;

    while (1) {
        receivedChar = serial.read();

        if (receivedChar == EOF) {
            // short delay
            vTaskDelay(1);
            continue;
        }

        if (!isspace(receivedChar)) {
            idx++;
            serial.write((char *) &receivedChar);
        } else if (isspace(receivedChar)) {
            if ((receivedChar == '\r') || (receivedChar == '\n'))
                serial.write("\r\n");
            else
                serial.write((char *) &receivedChar);

            // constructs a debug event and sends it to queue
            if (idx > 0) {
                debugEvent *event = new debugEvent();
                event->format = debugFormat[0];
                event->data[0] = idx;
                event->data[1] = xTaskGetTickCount();

                xQueueSendToBack(debugMessageQueue, &event, tickToWait);
            }

            idx = 0;
        }
    }
}

void vButtonTask(void *vParameters) {
    DigitalIoPin button(0, 17, DigitalIoPin::pullup, false);
    TickType_t tickToWait = configTICK_RATE_HZ / 10;

    TickType_t start = 0;

    while (1) {
        int buttonReading = button.read();

        if ((buttonReading == BUTTON_PRESSED)) {
            // Start counting the length of press
            if (start == 0)
                start = xTaskGetTickCount();

        } else if ((buttonReading != BUTTON_PRESSED) && (start > 0)) {
            // Stop counting the length of press, calculate the period
            // and send a debug event to queue
            TickType_t period = xTaskGetTickCount() - start;

            debugEvent *event = new debugEvent();
            event->format = debugFormat[1];
            event->data[0] = period;
            event->data[1] = start;

            xQueueSendToBack(debugMessageQueue, &event, tickToWait);

            start = 0;
        }

        // short delay
        vTaskDelay(1);
    }
}

void vDebugPrintTask(void *vParameters) {
    char buffer[64];
    debugEvent *event;

    while (1) {
        if (xQueueReceive(debugMessageQueue, &event, 0) == pdPASS) {
            snprintf(buffer, 64, event->format, event->data[0], event->data[1]);
            ITM_write(buffer);
            delete event;
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
