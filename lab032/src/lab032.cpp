#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define BUTTON_PRESSED 0
#define URGENT_VALUE 112
#define BUTTON_DELAY 10

void setupHardware();
void vRandNumberTask(void *vParameters);
void vButtonTask(void *vParameters);
void vSerialPrintTask(void *vParameters);

QueueHandle_t intQueue;
SerialLog serial;

int main(void) {

    setupHardware();
    intQueue = xQueueCreate(20, sizeof(int));

    if (intQueue != NULL) {
        xTaskCreate(vRandNumberTask, "Send Random Number Task",
                configMINIMAL_STACK_SIZE, NULL, (tskIDLE_PRIORITY + 1),
                (TaskHandle_t *) NULL);

        xTaskCreate(vButtonTask, "Button Task", configMINIMAL_STACK_SIZE, NULL,
                (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

        xTaskCreate(vSerialPrintTask, "Serial Print Task", 256, NULL,
                (tskIDLE_PRIORITY + 2), (TaskHandle_t *) NULL);
    }

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vRandNumberTask(void *vParameters) {
    int randNum;
    int delayPeriod;

    while (1) {
        randNum = xTaskGetTickCount() % 1000;
        delayPeriod = (xTaskGetTickCount() % 401) + 100;
        if (xQueueSendToBack(intQueue, &randNum, 0) == pdPASS)
            vTaskDelay(delayPeriod);
    }
}

void vButtonTask(void *vParameters) {
    DigitalIoPin button(0, 17, DigitalIoPin::pullup, false);
    int buttonValue = URGENT_VALUE;

    int buttonState;
    int lastButtonState = !(BUTTON_PRESSED);
    BaseType_t lastDebounceTime = 0;

    while (1) {
        // read the state of button
        int buttonReading = button.read();

        if (buttonReading != lastButtonState) {
            lastDebounceTime = xTaskGetTickCount();
        }

        // wait until the debounce delay ends
        if ((xTaskGetTickCount() - lastDebounceTime) > BUTTON_DELAY) {

            // save the current button state
            if (buttonReading != buttonState) {
                buttonState = buttonReading;

                // send 112 into queue when the button is pressed
                if (buttonState == BUTTON_PRESSED)
                    xQueueSendToFront(intQueue, &buttonValue, portMAX_DELAY);
            }
        }

        // save the reading for the next time
        lastButtonState = buttonReading;
    }
}

void vSerialPrintTask(void *vParameters) {
    int receivedValue;
    char buffer[15];

    while (1) {
        if (xQueueReceive(intQueue, &receivedValue, portMAX_DELAY) == pdPASS) {

            snprintf(buffer, 15, "%d %s \r\n", receivedValue,
                    (receivedValue == URGENT_VALUE) ? "Help me" : "");
            serial.write(buffer);

            // Delay for ~300ms
            vTaskDelay(configTICK_RATE_HZ / 3.333);
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
