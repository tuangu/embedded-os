#include "board.h"

#include "SerialLog.h"

#include "semphr.h"
#include "portmacro.h"

SerialLog::SerialLog() {
    serialMutex = xSemaphoreCreateMutex();
}

SerialLog::~SerialLog() {
    if (serialMutex != NULL) {
        vSemaphoreDelete(serialMutex);
    }
}

void SerialLog::write(char *description) {
    if (serialMutex != NULL) {
        if (xSemaphoreTake(serialMutex, portMAX_DELAY) == pdTRUE) {
            Board_UARTPutSTR(description);
            xSemaphoreGive(serialMutex);
        }
    }
}
