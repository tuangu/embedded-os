#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "DigitalIoPin.h"
#include "SerialLog.h"

#include "FreeRTOS.h"
#include "task.h"

#define BUTTON_PRESSED 0

void vSetupHardware();

SerialLog logger = SerialLog();

void vSwitch1Task(void *vParameters);
void vSwitch2Task(void *vParameters);
void vSwitch3Task(void *vParameters);

int main(void) {

    vSetupHardware();

    xTaskCreate(vSwitch1Task, "SW1 Task", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vSwitch2Task, "SW2 Task", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vSwitch3Task, "SW3 Task", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    return 0;
}

void vSetupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(1, true);
}

void vSwitch1Task(void *vParameters) {
    static DigitalIoPin button(0, 17, DigitalIoPin::pullup, 0);

    while (1) {
        if (button.read() == BUTTON_PRESSED) {
            logger.write("SW1 pressed \r\n");
        }
    }
}

void vSwitch2Task(void *vParameters) {
    static DigitalIoPin button(1, 11, DigitalIoPin::pullup, 0);

    while (1) {
        if (button.read() == BUTTON_PRESSED) {
            logger.write("SW2 pressed \r\n");
        }
    }
}

void vSwitch3Task(void *vParameters) {
    static DigitalIoPin button(1, 9, DigitalIoPin::pullup, 0);

    while (1) {
        if (button.read() == BUTTON_PRESSED) {
            logger.write("SW3 pressed \r\n");
        }

    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
