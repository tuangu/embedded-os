#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"

#define BUTTON_PRESSED 0

static void prvSetupHardware();
static void vLEDTask1(void *pvParameters);
static void vUARTTask(void *pvParameters);

DigitalIoPin button(0, 17, DigitalIoPin::input, false);

int main(void) {

    prvSetupHardware();

    xTaskCreate(vLEDTask1, "vTaskLed1", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vUARTTask, "vTaskUart", configMINIMAL_STACK_SIZE + 128, NULL,
            (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    // Start the scheduler
    vTaskStartScheduler();

    return 1;
}

static void prvSetupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
    Board_LED_Set(1, false);
}

static void vLEDTask1(void *pvParameters) {
    bool LedState = true;

    while (1) {
        Board_LED_Set(0, LedState);
        LedState = !LedState;

        vTaskDelay(configTICK_RATE_HZ / 5);
    }
}

static void vUARTTask(void *pvParameters) {
    int tickCnt = 0;
    int se = 0;
    int mi = 0;
    char buffer[6];

    while (1) {
        mi = tickCnt / 60;
        se = tickCnt - mi * 60;
        // minute ranges from 00:99
        snprintf(buffer, 9, "%02d:%02d", mi, se);
        DEBUGOUT("Tick: %s \r\n", buffer);
        if (button.read() == BUTTON_PRESSED)
            tickCnt += 10;
        else
            tickCnt++;

        vTaskDelay(configTICK_RATE_HZ);
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
