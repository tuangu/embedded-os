#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdlib.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

#define BUTTON_BIT(c) (1 << (c))
const EventBits_t bitToWait = BUTTON_BIT(1) | BUTTON_BIT(2) | BUTTON_BIT(3);

void setupHardware();

void vTask1(void *vParameters);
void vTask2(void *vParameters);
void vTask3(void *vParameters);

SerialLog serial;
EventGroupHandle_t eBtn;


int main(void) {

    setupHardware();

    eBtn = xEventGroupCreate();

    xTaskCreate(vTask1, "Task 1", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask2, "Task 2", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask3, "Task 3", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vTask1(void *vParameters) {
    DigitalIoPin btn1(0, 17, DigitalIoPin::pullup, true);
    uint8_t count = 1;

    while (1) {
        if (btn1.read())
            count--;

        if (count == 0) {
            xEventGroupSetBits(eBtn, BUTTON_BIT(1));
            break;
        }

        vTaskDelay(configTICK_RATE_HZ / 10);
    }

    TickType_t tickCount = xTaskGetTickCount();
    EventBits_t eventBit;

    while (1) {
        eventBit = xEventGroupWaitBits(eBtn, bitToWait, pdFALSE, pdTRUE, portMAX_DELAY);

        if (eventBit == bitToWait) {
            char buffer[16];
            snprintf(buffer, 16, "Task 1 %lu\r\n", tickCount);
            serial.write(buffer);
            break;
        }
    }

    while (1);
}

void vTask2(void *vParameters) {
    DigitalIoPin btn2(1, 11, DigitalIoPin::pullup, true);
    uint8_t count = 2;

    while (1) {
        if (btn2.read())
            count--;

        if (count == 0) {
            xEventGroupSetBits(eBtn, BUTTON_BIT(2));
            break;
        }

        vTaskDelay(configTICK_RATE_HZ / 10);
    }

    TickType_t tickCount = xTaskGetTickCount();
    EventBits_t eventBit;

    while (1) {
        eventBit = xEventGroupWaitBits(eBtn, bitToWait, pdFALSE, pdTRUE, portMAX_DELAY);

        if (eventBit == bitToWait) {
            char buffer[16];
            snprintf(buffer, 16, "Task 2 %lu\r\n", tickCount);
            serial.write(buffer);
            break;
        }
    }

    while (1);
}

void vTask3(void *vParameters) {
    DigitalIoPin btn3(1, 9, DigitalIoPin::pullup, true);
    uint8_t count = 3;

    while (1) {
        if (btn3.read())
            count--;

        if (count == 0) {
            xEventGroupSetBits(eBtn, BUTTON_BIT(3));
            break;
        }

        vTaskDelay(configTICK_RATE_HZ / 10);
    }

    TickType_t tickCount = xTaskGetTickCount();
    EventBits_t eventBit;

    while (1) {
        eventBit = xEventGroupWaitBits(eBtn, bitToWait, pdFALSE, pdTRUE, portMAX_DELAY);

        if (eventBit == bitToWait) {
            char buffer[16];
            snprintf(buffer, 16, "Task 3 %lu\r\n", tickCount);
            serial.write(buffer);
            break;
        }
    }

    while (1);
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
