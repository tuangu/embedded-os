#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include <stdio.h>

#include "DigitalIoPin.h"
#include "SerialLog.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define LED_RED 0
#define LED_GREEN 1
#define LED_BLUE 2

#define LEFT_TO_RIGHT 0

#define MOTOR_DELAY 3

void setupHardware();
void vLimitSwitchTask(void *vParameters);
void vMotorTask(void *vParameters);

SemaphoreHandle_t motorSemphr;
SerialLog logger;
DigitalIoPin lm1(0, 27, DigitalIoPin::pullup, true);
DigitalIoPin lm2(0, 28, DigitalIoPin::pullup, true);

int main(void) {

    setupHardware();

    motorSemphr = xSemaphoreCreateBinary();

    xTaskCreate(vLimitSwitchTask, "Limit Switch Task", configMINIMAL_STACK_SIZE,
            NULL, (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

    xTaskCreate(vMotorTask, "Motor Task", configMINIMAL_STACK_SIZE * 4, NULL,
            (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

    logger.write("Start...\r\n");
    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
    Board_LED_Set(1, false);
}

/*
 *  1. When task starts it reads limit switches and waits until both limit switches are open. The blue led
 *	blinks during waiting.
 *	2. Then task signals “go” using binary semaphore
 *	3. Then task enters a loop where it reads limit switches and switches red led on for 1s if limit switch 1 is
 *	closed and green led on for 1s if limit switch 2 is closed.
 */
void vLimitSwitchTask(void *vParameters) {

    TickType_t tickToBlink = 100;
    bool ledState = true;

    while (lm1.read() & lm2.read()) {
        Board_LED_Set(LED_BLUE, ledState);
        ledState = !ledState;
        vTaskDelay(tickToBlink);
    }

    Board_LED_Set(LED_BLUE, false);
    xSemaphoreGive(motorSemphr);

    while (1) {
        bool lm1Reading = lm1.read();
        bool lm2Reading = lm2.read();

        if (!lm1Reading && !lm2Reading) {
            Board_LED_Set(LED_GREEN, false);
            Board_LED_Set(LED_RED, false);
        } else if (lm1Reading) {
            Board_LED_Set(LED_RED, true);
            vTaskDelay(configTICK_RATE_HZ);
        } else if (lm2Reading) {
            Board_LED_Set(LED_GREEN, true);
            vTaskDelay(configTICK_RATE_HZ);
        } else {
            Board_LED_Set(LED_GREEN, false);
            Board_LED_Set(LED_RED, false);
        }
    }
}

/*
 * 	1. Task waits on the binary semaphore. When it gets the semaphore (“go” signal) the task continues
 * 	2. Task starts to run stepper motor in either direction and runs the motor at constant speed until one
 *	of the limit switches is closed. Then the motor direction is toggled. Program must count the
 *	number of steps between limit switches. You may do multiple runs and use averaging to get
 *	accurate value
 *	3. When the number of steps between the limit switches is known the task switches the blue led on
 *	for two seconds. Then the blue led is switched off and task starts to run motor back and forth so
 *	that it does not hit either of the limit switches. Note that there is some hysteresis in the limit
 *	switches. When switch is closed you need to back out more than just one step to open the switch
 *	again.
 */
void vMotorTask(void *vParameters) {
    DigitalIoPin stepPin(0, 24, DigitalIoPin::output, false);
    DigitalIoPin directionPin(1, 0, DigitalIoPin::output, false);

    /*
     * mode = 0: count the number of steps between limit switches
     * mode = 1: the motor runs back and forth
     */
    bool mode = 0;
    char index = 0;

    bool direction = LEFT_TO_RIGHT;
    bool step = 0;

    int minStep = 30000;
    long prevCount = 0;
    long currCount = 0;

    xSemaphoreTake(motorSemphr, portMAX_DELAY);

    while (1) {
        if (mode) {
            int stepToRun = 2 * minStep - 60;
            step = 0;
            while (stepToRun-- >= 0) {
                stepPin.write(step);
                step = !step;
                --stepToRun;
                vTaskDelay(MOTOR_DELAY);
            }

            direction = !direction;
            directionPin.write(direction);
        } else {
            stepPin.write(step);
            step = !step;
            vTaskDelay(MOTOR_DELAY);
            ++currCount;

            if (((direction == LEFT_TO_RIGHT) && lm2.read())
                    || ((direction != LEFT_TO_RIGHT) && lm1.read())) {
                direction = !direction;
                directionPin.write(direction);

                int stepTemp = currCount - prevCount;
                if ((index > 1) && (stepTemp < minStep))
                    minStep = stepTemp;

                char message[15];
                snprintf(message, 30, "#%d: %d\r\n", index, stepTemp);
                logger.write(message);

                prevCount = currCount;
                ++index;
            }

            /* At the end of the measuring job, move the motor until both limit switches release */
            if (index == 5) {
                const BaseType_t debounceDelay = 50;
                bool lastLmState = 0;
                BaseType_t lastDebounceTime = 0;

                while (1) {
                    bool lmReading;
                    if (direction == LEFT_TO_RIGHT)
                        lmReading = lm1.read();
                    else
                        lmReading = lm2.read();

                    if (lmReading != lastLmState)
                        lastDebounceTime = xTaskGetTickCount();

                    if ((xTaskGetTickCount() - lastDebounceTime)
                            > debounceDelay) {
                        // Stop if the limit switch is released
                        if (!lmReading)
                            break;
                    } else {
                        stepPin.write(step);
                        step = !step;
                        vTaskDelay(MOTOR_DELAY);
                    }

                    lastLmState = lmReading;
                }

                mode = 1;
                Board_LED_Set(2, true);
                vTaskDelay(2000);
                Board_LED_Set(2, false);
            }
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
