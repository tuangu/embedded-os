/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "SerialLog.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define SERIAL_BUFFER_SIZE 25

void setupHardware();

void vSerialTask(void *vParameters);
void vIndicatorTask(void *vParameters);

SemaphoreHandle_t binarySemaphore;
SerialLog logger;

int main(void) {

    setupHardware();

    binarySemaphore = xSemaphoreCreateBinary();

    xTaskCreate(vSerialTask, "Serial Task", 128, NULL, (tskIDLE_PRIORITY + 1UL),
            (TaskHandle_t *) NULL);

    xTaskCreate(vIndicatorTask, "Indicator Task", configMINIMAL_STACK_SIZE,
            NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vSerialTask(void *vParameters) {

    while (1) {
        int c = logger.read();
        if ((c != EOF) && (c != '\r')) {
            logger.write((char *) &c);
            xSemaphoreGive(binarySemaphore);
        } else if (c == '\r') {
            logger.write("\r\n");
            xSemaphoreGive(binarySemaphore);
        }
    }
}

void vIndicatorTask(void *vParameters) {

    while (1) {
        if (xSemaphoreTake(binarySemaphore, portMAX_DELAY) == pdPASS) {
            Board_LED_Set(0, true);
            vTaskDelay(configTICK_RATE_HZ / 10);
            Board_LED_Set(0, false);
            vTaskDelay(configTICK_RATE_HZ / 10);
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
