#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>
#include <ctype.h>

#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define BUTTON_PRESSED 0

void setupHardware();
void vLimitSwitchTask(void *vParameters);
void vMotorTask(void *vParameters);

SemaphoreHandle_t motorMutex;

int main(void) {

    setupHardware();

    motorMutex = xSemaphoreCreateMutex();
    xSemaphoreGive(motorMutex);

    xTaskCreate(vLimitSwitchTask, "Limit Switch Task", configMINIMAL_STACK_SIZE,
            NULL, (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

    xTaskCreate(vMotorTask, "Motor Task", configMINIMAL_STACK_SIZE, NULL,
            (tskIDLE_PRIORITY + 1), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
    Board_LED_Set(1, false);
}

void vLimitSwitchTask(void *vParameters) {
    DigitalIoPin sw1(0, 27, DigitalIoPin::pullup, false);
    DigitalIoPin sw2(0, 28, DigitalIoPin::pullup, false);

    while (1) {
        bool sw1Reading = sw1.read();
        bool sw2Reading = sw2.read();

        if (sw1Reading == BUTTON_PRESSED) {
            Board_LED_Set(0, true);
            xSemaphoreTake(motorMutex, 0);
        } else if (sw2Reading == BUTTON_PRESSED) {
            Board_LED_Set(1, true);
            xSemaphoreTake(motorMutex, 0);
        } else {
            Board_LED_Set(0, false);
            Board_LED_Set(1, false);
            xSemaphoreGive(motorMutex);
        }
    }
}

void vMotorTask(void *vParameters) {
    DigitalIoPin stepPin(0, 24, DigitalIoPin::output, false);
    DigitalIoPin directionPin(1, 0, DigitalIoPin::output, false);
    DigitalIoPin sw1(0, 17, DigitalIoPin::pullup, false);
    DigitalIoPin sw3(1, 9, DigitalIoPin::pullup, false);
    DigitalIoPin lm1(0, 27, DigitalIoPin::pullup, false);
    DigitalIoPin lm2(0, 28, DigitalIoPin::pullup, false);

    bool direction;
    bool lastDirection = false;
    bool step = 0;

    directionPin.write(lastDirection);

    while (1) {
        if (xSemaphoreTake(motorMutex, 0) == pdTRUE) {
            bool lm1Reading = lm1.read();
            bool lm2Reading = lm2.read();

            if (!lm1Reading || !lm2Reading) {
                xSemaphoreGive(motorMutex);
                continue;
            }

            bool sw1Reading = sw1.read();
            bool sw3Reading = sw3.read();

            if (sw1Reading == BUTTON_PRESSED)
                direction = 1;
            else if (sw3Reading == BUTTON_PRESSED)
                direction = 0;

            if (direction != lastDirection) {
                directionPin.write(direction);
                step = 1;
            }

            if (sw1Reading ^ sw3Reading) {
                stepPin.write(step);
                step = !step;
                vTaskDelay(1);
            }

            lastDirection = direction;
        }
    }
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
