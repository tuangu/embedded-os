#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdlib.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"

void setupHardware();
void oneShootCallback(TimerHandle_t timer);
void autoReloadCallback(TimerHandle_t timer);

void vTask1(void *vParameters);
void vTask2(void *vParameters);

SerialLog serial;
SemaphoreHandle_t sbOneShoot;
QueueHandle_t qPrint;
TimerHandle_t tOneShoot;
TimerHandle_t tAutoReload;

int main(void) {

    setupHardware();

    sbOneShoot = xSemaphoreCreateBinary();
    qPrint = xQueueCreate(10, sizeof(int));

    tOneShoot = xTimerCreate("One Shoot Timer", 20 * configTICK_RATE_HZ,
                             pdFALSE, (void *) 0, oneShootCallback);

    tAutoReload = xTimerCreate("Auto Reload Timer", 5 * configTICK_RATE_HZ,
                               pdTRUE, (void *) 0, autoReloadCallback);

    xTaskCreate(vTask1, "Task 1", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    xTaskCreate(vTask2, "Task 2", configMINIMAL_STACK_SIZE,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vTask1(void *vParameters) {

    int recv = 0;

    while (1) {
        xQueueReceive(qPrint, &recv, portMAX_DELAY);
        if (recv == 1)
            serial.write("Aargh\r\n");
        else if (recv == 2)
            serial.write("Hello\r\n");
    }
}

void vTask2(void *vParameters) {

    xTimerStart(tOneShoot, portMAX_DELAY);
    xTimerStart(tAutoReload, portMAX_DELAY);

    xSemaphoreTake(sbOneShoot, portMAX_DELAY);

    int cmd = 1;
    xQueueSendToBack(qPrint, &cmd, portMAX_DELAY);

    while (1) {
    }
}

void oneShootCallback(TimerHandle_t timer) {
    xSemaphoreGive(sbOneShoot);
}

void autoReloadCallback(TimerHandle_t timer) {
    int cmd = 2;
    xQueueSendToBack(qPrint, &cmd, 0);
}

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
