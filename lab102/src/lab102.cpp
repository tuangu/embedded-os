#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "SerialLog.h"
#include "DigitalIoPin.h"

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

void setupHardware();
void timerCallback(TimerHandle_t timer);

void vTask1(void *vParameters);
void vTask2(void *vParameters);

SerialLog serial;
TimerHandle_t tOneShoot;
int main(void) {

    setupHardware();

    tOneShoot = xTimerCreate("One Shoot Timer", 5 * configTICK_RATE_HZ,
                             pdFALSE, 0, timerCallback);

    xTaskCreate(vTask1, "Task 1", configMINIMAL_STACK_SIZE * 2,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    while (1)
        ;

    return 0;
}

void setupHardware() {
    SystemCoreClockUpdate();
    Board_Init();

    Board_LED_Set(0, false);
}

void vTask1(void *vParameters) {
    DigitalIoPin btn1(0, 17,  DigitalIoPin::pullup, true);
    DigitalIoPin btn2(1, 11,  DigitalIoPin::pullup, true);
    DigitalIoPin btn3(1, 9,  DigitalIoPin::pullup, true);

    while (1) {
        if (btn1.read() || btn2.read() || btn3.read()) {
            Board_LED_Set(1, true);
            xTimerReset(tOneShoot, portMAX_DELAY);
        }
        vTaskDelay(20);
    }
}

void timerCallback(TimerHandle_t timer) {
    // Turn off green LED
    Board_LED_Set(1, false);
}
/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
