#ifndef DEBUGEVENT_H_
#define DEBUGEVENT_H_

struct debugEvent {
    char *format;
    uint32_t data[3];
};

#endif /* DEBUGEVENT_H_ */
