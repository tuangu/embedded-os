#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include <stdio.h>

#include "DigitalIoPin.h"
#include "ITM_write.h"

#include "FreeRTOS.h"
#include "task.h"

void vSetupHardware();
void SCT0_Setup();

void vLedTask(void *vParameters);

int main(void) {

    vSetupHardware();

    xTaskCreate(vLedTask, "LED Task", configMINIMAL_STACK_SIZE * 3,
                NULL, (tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

    vTaskStartScheduler();

    return 0;
}

void vSetupHardware() {
    SystemCoreClockUpdate();

    Board_Init();
    Board_LED_Set(2, false);
    Board_LED_Set(1, false);
    Board_LED_Set(0, false);

    ITM_init();

    Chip_SCT_Init(LPC_SCT0);
    SCT0_Setup();

    Chip_SWM_Init();
    Chip_SWM_MovablePortPinAssign(SWM_SCT0_OUT0_O, 0, 3);
}

void SCT0_Setup() {
    LPC_SCT0->CONFIG |= (1 << 17); // two 16-bit timers, auto limit
    LPC_SCT0->CTRL_L |= (72-1) << 5; // set prescaler, SCTimer/PWM clock = 1 MHz

    /* The on-board leds work with inverted logic. */
    LPC_SCT0->MATCHREL[0].L = 1000-1; // match 0 @ 1 kHz PWM freq
    LPC_SCT0->MATCHREL[1].L = 950; // match 1 used for duty cycle, = 5%

    LPC_SCT0->EVENT[0].STATE = 0xFFFFFFFF; // event 0 happens in all states
    LPC_SCT0->EVENT[0].CTRL = (1 << 12); // match 0 condition only

    LPC_SCT0->EVENT[1].STATE = 0xFFFFFFFF; // event 1 happens in all states
    LPC_SCT0->EVENT[1].CTRL = (1 << 0) | (1 << 12); // match 1 condition only

    LPC_SCT0->OUT[0].SET = (1 << 0); // event 0 will set SCTx_OUT0
    LPC_SCT0->OUT[0].CLR = (1 << 1); // event 1 will clear SCTx_OUT0

    LPC_SCT0->CTRL_L &= ~(1 << 2); // unhalt it by clearing bit 2 of CTRL reg
}

void vLedTask(void *vParameters) {
    DigitalIoPin sw1(0, 17, DigitalIoPin::pullup, true);
    DigitalIoPin sw2(1, 11, DigitalIoPin::pullup, true);
    DigitalIoPin sw3(1, 9, DigitalIoPin::pullup, true);

    const bool BUTTON_PRESSED = 1;

    const uint8_t SLOW_RATE = 50;
    const uint8_t RAPID_RATE = 200;

    while (1) {
        bool sw1Read = sw1.read();
        bool sw2Read = sw2.read();
        bool sw3Read = sw3.read();

        if (sw2Read == BUTTON_PRESSED) {
            if (sw1Read == BUTTON_PRESSED) { // increase duty cycle
                LPC_SCT0->MATCHREL[1].L -= (LPC_SCT0->MATCHREL[1].L >= RAPID_RATE) ? RAPID_RATE : 0;
            } else if (sw3Read == BUTTON_PRESSED) { // decrease duty cycle
                LPC_SCT0->MATCHREL[1].L += (LPC_SCT0->MATCHREL[1].L <= (1000 - RAPID_RATE)) ? RAPID_RATE : 0;
            }
        } else if (sw1Read == BUTTON_PRESSED) {
            // increase duty cycle
            LPC_SCT0->MATCHREL[1].L -= (LPC_SCT0->MATCHREL[1].L >= SLOW_RATE) ? SLOW_RATE : 0;
        } else if (sw3Read == BUTTON_PRESSED) {
            // decrease duty cycle
            LPC_SCT0->MATCHREL[1].L += (LPC_SCT0->MATCHREL[1].L <= (1000 - SLOW_RATE)) ? SLOW_RATE : 0;
        }

        char buffer[20];
        sniprintf(buffer, 20, "PWM: %d %\r\n", 100 - LPC_SCT0->MATCHREL[1].L / 10 );
        ITM_write(buffer);

        vTaskDelay(100);
    }
}


/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats(void) {
    Chip_SCT_Init(LPC_SCTSMALL1);
    LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
    LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statistics collection */
