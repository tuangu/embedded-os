#ifndef BUTTONEVENT_H_
#define BUTTONEVENT_H_

#include "FreeRTOS.h"

struct ButtonEvent {
    uint8_t pin;
    BaseType_t tickCount;
};



#endif /* BUTTONEVENT_H_ */
